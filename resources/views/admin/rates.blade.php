@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
				<span>Dashboard / Rates</span>		
				</div>

                <div class="panel-body">
				
					<h3>Import currency rates</h3>
					<span> from 01.01.2000 to 01.01.2017</span><hr>
					{{-- Check if has any currencies --}}
					@if(count($currencies)>0)
						<div class="row">
							<div class="col-xs-12">
								<label for="currencySelector">Select Currency</label>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-sm-7 form-group">
							
								{{-- Loop through currencies --}}
								<select class="form-control" id="currencySelector" >
									@foreach($currencies as $currency)
									<option value="{{$currency->code}}">
									{{$currency->code}}
									</option>
									@endforeach
								</select>
								
							</div>
							<div class="col-xs-12 col-sm-5">
								<a class="btn btn-success" id="getRates">Get rates</a>
							<a href="/currencies" class="btn btn-info pull-right">Manage Currencies</a>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12">
								<ul class="results"></ul>
							</div>
						</div>
					@endif
					
					@if(count($currencies)==0)
                        <div class="alert alert-info">
						<b>Note</b><br>
							You don't have any currencies yet.
                        </div>
						<a href="/currencies" class="btn btn-success">Create currency</a>
					@endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page_js')
    <style>
	.exists { color: lightslategrey; }
	.success { color: limegreen; }
	.error { color: orange; }
    </style>
    <script>
	var dates = {!!$dates!!};
	var currencies = {!!$currencies!!};
	var code = $('#currencySelector').find('option:selected').val();
	$('#currencySelector').on('change',function() {
		code = $(this).find('option:selected').val();
	});
	$('#getRates').on('click', function(){
		$('.results').html('');
		dates.forEach(function(date, key){
			
			var li = '<li id="li-' + date + '">' + code + ' (' + date + ') importing...</li>';
			$('.results').append(li);
			$.ajax({
				url: "rates/store/" + code + "/" + date, 
				success: function(result){
					$('#li-' + date).html(result.message).addClass(result.status);
					
				}
			});
		});
	});
	</script>
@endsection
