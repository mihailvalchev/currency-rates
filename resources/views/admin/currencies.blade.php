@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
				<span>Dashboard / Currencies</span>				
				</div>

                <div class="panel-body">
				
					@if ( count( $errors ) > 0 )
                        <div class="alert alert-danger">
						<b>Error.</b><br>
						@foreach ($errors->all() as $error)
								{!! $error !!}<br>
						@endforeach
                        </div>
					@endif
					@if ( Session::has('success') )
                        <div class="alert alert-success">
						<b>Success.</b><br>
							{{ Session::get('success')}}
                        </div>
					@endif
					
					<h3>Import</h3>
					<a class="btn btn-success btn-sm" href="/currencies/import">Auto Import</a>
					<a href="/set_rates" class="btn btn-warning pull-right">Import Rates</a>
					
					<h3>Add new</h3>
					{{-- Loop through currencies --}}
					<table class="table table-stripped">		
						<thead>
						  <tr>
							<th>Currency Name</th>
							<th>Code</th>
							<th>Actions</th>
						  </tr>
						</thead>
						<form action="/currencies/create" method="POST">
								<tr>
									<td>{{ csrf_field() }}<input placeholder="Currency Name" type="text" class="form-control" name="name" value="" /></td>
									<td><input placeholder="Code" type="text" class="form-control" name="code" value="" /></td>
									<td><button class="btn btn-success btn-sm" type="submit">Add New</button></td>
								</tr>
						</form>
						@if(count($currencies)>0)
						{{-- Check if has any currencies --}}
						<form action="/currencies/edit" method="POST">
							{{ csrf_field() }}
							@foreach($currencies as $currency)
								<tr>
									<td><input type="text" class="form-control" name="currencies[{{$currency->id}}][name]" value="{{$currency->name}}" /></td>
									<td><input type="text" class="form-control" name="currencies[{{$currency->id}}][code]" value="{{$currency->code}}" /></td>
									<td><a class="btn btn-danger btn-sm" href="/currencies/delete/{{$currency->id}}">Delete</a></td>
								</tr>
							@endforeach
								<tr>
									<td></td>
									<td></td>
									<td><button class="btn btn-success btn-sm" type="submit">Save Changes</button></td>
								</tr>
						</form>									
						@endif
					</table>
					
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
