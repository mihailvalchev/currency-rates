@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
					Home
					<div class="pull-right bold">Dashboard</div>
				</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
					<div class="row">
						<div class="col-xs-12">
							<a href="/set_rates" class="btn btn-warning">Import Rates</a>
							<a href="/currencies" class="btn btn-info">Manage Currencies</a>
						</div>
					</div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
