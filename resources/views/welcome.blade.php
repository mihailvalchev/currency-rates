@extends('layouts.app')

@section('content')

<!-- Styles -->
<style>
    html, body {
        background-color: #fff;
        color: #636b6f;
        font-family: 'Raleway', sans-serif;
        font-weight: 300;
        height: 100vh;
        margin: 0;
    }

    .flex-center {
        align-items: center;
        display: flex;
        justify-content: center;
    }

    .position-ref {
        position: relative;
    }

    .top-right {
        position: absolute;
        right: 10px;
        top: 18px;
    }

    .content {
        text-align: center;
    }

    .title {
        font-size: 84px;
    }

    .links > a {
        color: #636b6f;
        padding: 0 25px;
        font-size: 12px;
        font-weight: 600;
        letter-spacing: .1rem;
        text-decoration: none;
        text-transform: uppercase;
    }

    .m-b-md {
        margin-bottom: 30px;
    }
</style>
<div class="flex-center position-ref">

    <div class="content">
        <div class="title m-b-md">
            Rates
        </div>
		<span>2000-2017</span><br>
		@if(count($currencies)>0)
			<h3>Your Currencies</h3>
			<div class="row">
				<div class="col-xs-12">
					<table class="table table-stripped">
						
						@foreach($currencies as $currency)
							@if(count($currency['rates'])>0)
							<tr>
								<td>{{$currency->name}}</td>
								<td><b>{{$currency->code}}</b></td>
								<td><a href="/currency/{{$currency->code}}" class="btn btn-success btn-xs">Details</a></td>
							</tr>
							@endif
						@endforeach
					</table>
				</div>
			</div>
		@else
			<h3>You don't have any currencies yet</h3>
			<span><a href="/register">Register</a> and import currencies</span>
		@endif
    </div>
</div>
@endsection
