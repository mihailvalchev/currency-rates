@extends('layouts.app')

@section('content')

<!-- Styles -->
<style>
	th, td {		
		font-size: 0.8em;
		padding: 5px 0px !important;
		text-align: center;
		color: #000;
		vertical-align: middle !important;
	}
</style>
<div class="flex-center position-ref">

    <div class="content">
		@if(isset($currency))
			<div class="row">
				<div class="col-xs-8 col-xs-offset-2">				
					<canvas id="canvas"></canvas>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<table class="table table-bordered table-striped table-hover">
					<thead>
						<tr>		
							<th></th>		
							@foreach($rate_keys as $rate_key)
								<th>{{$rate_key}}</th>
							@endforeach		
						</tr>
					</thead>
					<tbody>
						@foreach($currency->rates as $rate)
						<tr>	
								<td>{{ $rate['date'] }}</td>
								@foreach($rate_keys as $rate_key)
									<?php $value = isset(json_decode($rate['value'],true)['rates'][$rate_key]) ? json_decode($rate['value'],true)['rates'][$rate_key] : '-'; ?>
									<?php $valuee = json_decode($rate['value'],true); ?>
									<td>{{ $value }}</td>
								@endforeach
						</tr>
						@endforeach
					</tbody>
					</table>
					@foreach($currency->rates as $rate)
					@endforeach
				
				</div>
			</div>
		@else
			<h3>Not Available...</h3>
		@endif
    </div>
</div>
<script src="/js/Chart.bundle.js"></script>
<script src="/js/utils.js"></script>
	<style>
		canvas {
			width: 75%;
			height: 75vh;
		}
	</style><script src="https://codepen.io/anon/pen/aWapBE.js"></script>
    <script>
        var MONTHS = {!!json_encode($graphData['dates'])!!};
        var config = {
            type: 'line',
            data: {
                labels: {!!json_encode($graphData['dates'])!!},
                datasets: [
				
				@foreach($rate_keys as $rate)
					{
						label:"{{$rate}}",
						backgroundColor: color = '#'+(Math.random()*0xFFFFFF<<0).toString(16),
						borderColor: color,
						data: [
						@foreach($graphData['values'] as $data)
								{{ isset($data[$rate]) ? $data[$rate] : 0 }},
						@endforeach 
						],
						fill: false,
					},
				@endforeach 
				
				]
            },
            options: {
                responsive: true,
                title:{
                    display:true,
                    text:'{{$currency->name}} Details'
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Date'
                        }
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Value'
                        }
                    }]
                }
            }
        };

        window.onload = function() {
            var ctx = document.getElementById("canvas").getContext("2d");
            window.myLine = new Chart(ctx, config);
        };


        var colorNames = Object.keys(window.chartColors);
    </script>
@endsection

@section('page_js')

@endsection