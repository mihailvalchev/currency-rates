<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FrontendController@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/currency/{code}', 'HomeController@currency')->name('currency');

Route::group(['middleware' => 'auth'], function () {

    //Logged in
	
	//Rates
	Route::get('/set_rates', 'RatesController@index')->name('rates');
	Route::post('/rates/import', 'RatesController@import');
	Route::get('/rates/delete/{currency_code}', 'RatesController@delete');
	
	//Currencies
	Route::get('/currencies', 'CurrenciesController@index')->name('currencies');
	Route::post('/currencies/create', 'CurrenciesController@create');
	Route::post('/currencies/edit', 'CurrenciesController@edit');
	Route::get('/currencies/delete/{id}', 'CurrenciesController@delete');
	Route::get('/currencies/import', 'CurrenciesController@import');
	
	
	Route::get('/rates/store/{code}/{date}', 'RatesController@store');

});