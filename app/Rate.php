<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rate extends Model
{
    protected $fillable = ['value', 'date'];
	
	public function currency()
	{
	  return $this->hasOne('Currency');
	}
}
