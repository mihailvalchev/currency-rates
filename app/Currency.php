<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    protected $fillable = ['name', 'code'];

    /**
     *  User type relation
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function rates()
    {
        return $this->hasMany('App\Rate');
    }
}
