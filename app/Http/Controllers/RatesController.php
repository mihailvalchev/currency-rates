<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Currency;
use App\Rate;
use App\Http\Requests\CreateCurrencyRequest;
use App\Http\Requests\EditCurrencyRequest;
use GuzzleHttp\Client;

use Carbon\Carbon;

class RatesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the currency rates page
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$currencies = Currency::orderBy('name', 'ASC')->get();
		$from = Carbon::createFromDate(2000, 1, 1);
		$to = Carbon::createFromDate(2017, 1, 1);
		$dates = json_encode($this->generateDateRange( $from, $to ));
		
        return view('admin/rates', compact(['currencies', 'dates']));
    }
	
    /**
     * Get Rate
     *
     * @return \Illuminate\Http\Response
     */
    public function store($code, $date)
    {
		$client = new Client();
		$currency = Currency::where('code', $code)->first();
		
		
		//Check if rate exists
		$find_rate = Rate::where('date', $date)->where('currency_id', $currency->id)->count();
		
		if($find_rate == 0){
			try {
					//Api call
					$rate = $client->get('http://api.fixer.io/' . $date . '?base=' . $code);
					$data = $rate->getBody()->getContents();	
					
					//Create New rate
					$rates = new Rate();
					$rates->value = $data;
					$rates->date = $date;
					$rates->currency_id = $currency->id;
					$rates->save();
					
					return response()->json([ 'status' => 'success' , 'message' => $code . ' (' . $date . ')  was imported successfully.']);		
					
			
			} catch (\Exception $e) {
				return response()->json([ 'status' => 'error' , 'message' => $code . ' (' . $date . ') was not imported for some reason. (<a href="http://api.fixer.io/' . $date . '?base=' . $code.'" target="_blank">Fixer Api Url</a>)']);
			}
		}else{
			
			return response()->json([ 'status' => 'exists' , 'message' => $code . ' (' . $date . ') already exists']);
		}
		
		
    }
	

    /**
     * Generate date range
     *
     * @return array
     */
	private function generateDateRange(Carbon $start_date, Carbon $end_date)
	{
		$dates = [];

		for($date = $start_date; $date->lte($end_date); $date->addMonths(6)) {
			$dates[] = $date->format('Y-m-d');
		}

		return $dates;
	}
	
}
