<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Currency;
use Auth;
use Carbon\Carbon;

class FrontendController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		//dd(Currency::first()->rates());
		$currencies = Currency::whereHas('rates')->with('rates')->orderBy('name', 'ASC')->get();
		
        return view('welcome', compact(['currencies']));
    }

}
