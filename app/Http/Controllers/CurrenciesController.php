<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Currency;
use App\Http\Requests\CreateCurrencyRequest;
use App\Http\Requests\EditCurrencyRequest;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Schema;

class CurrenciesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the currencies page
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$currencies = Currency::orderBy('name', 'ASC')->get();
        return view('admin/currencies', compact(['currencies']));
    }

    /**
     * Create currency
     *
     */
    public function create(CreateCurrencyRequest $request)
    {
		$postData = $request->all();
		unset($postData['_token']);
		
		Currency::create($postData);
        return redirect()->back()->with(['success' => '"' . $postData['name'] . '" was created.']);
    }

    /**
     * Update currencies
     *
     */
    public function edit(EditCurrencyRequest $request)
    {
		$postData = $request->all();
		
		foreach($postData['currencies'] as $id => $row){
			Currency::find($id)->update($row);
		}
        return redirect()->back()->with(['success' => 'The currencies were updated.']);
    }

    /**
     * Delete currency
     *
     */
    public function delete($id)
    {
		Currency::find($id)->delete();
        return redirect()->back()->with(['success' => 'The currency was deleted.']);
		
    }

    /**
     * Import currencies
     *
     */
    public function import()
    {
		
        $client = new Client();
		$res = $client->get('http://api.fixer.io/latest');
		$data = json_decode($res->getBody()->getContents(), true);
		
		$currencies = [];
		$currencies[] = $data['base'];
		foreach($data['rates'] as $code => $value){
			$currencies[] = $code;
		}
		$cols = [];
		$i = 0;
		foreach($currencies as $row){
			
			$row_count = Currency::where('code', $row)->count();
			if($row_count == 0){
				$currency = new Currency;
				$currency->name = $row;
				$currency->code = $row;
				$currency->save();
				$i++;
			}
		}
        return redirect()->back()->with(['success' => $i . ' currencies were imported.']);
    }
}
