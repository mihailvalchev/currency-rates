<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Currency;
use App\Rate;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application Home Page.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin/home');
    }

    /**
     * Show the application Currency Details Page.
     *
     * @return \Illuminate\Http\Response
     */
    public function currency($code)
    {
		$currency = Currency::where('code', $code)->with('rates')->first();
		
		//get latest rates added to the db, as it has the most keys
		$rate_keys = array_keys(json_decode(Rate::orderBy('created_at', 'desc')->first()['value'], true)['rates']);
		
		//Deleted IDR as it has abnormal values
		$delete_IDR = array_search('IDR', $rate_keys);
		unset($rate_keys[$delete_IDR]);
		
		$graphData= [];
		foreach($currency->toArray()['rates'] as $data){
			
			$date_rates = json_decode($data['value'], true)['rates'];
			
			$graphData['dates'][] = $data['date'];
			$graphData['values'][$data['date']] = $date_rates;
		}
		
        return view('currency', compact('currency', 'rate_keys', 'graphData'));
    }

}
