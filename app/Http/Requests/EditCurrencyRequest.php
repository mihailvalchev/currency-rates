<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditCurrencyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
			'currencies.*.name' => 'required',
			'currencies.*.code' => 'required'
        ];
    }
	/**
     * Modify response format
     *
     * @param array $errors
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function response(array $errors)
    {		
		/** Retrieves error message */
        return redirect()->back();
    }
}
